import { TeestPage } from './app.po';

describe('teest App', () => {
  let page: TeestPage;

  beforeEach(() => {
    page = new TeestPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
