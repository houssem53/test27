import {UIRouter} from "@uirouter/angular";
import {Injector, Injectable} from "@angular/core";

/** UIRouter Config  */
export function uiRouterConfigFn(router: UIRouter, injector: Injector) {
  
  // Plunker embeds can time out.  
  // Pre-load the people list at startup.
  
  // If no URL matches, go to the `hello` state by default
  router.urlService.rules.otherwise({ state: 'login' });
  
  // Use ui-router-visualizer to show the states as a tree
}