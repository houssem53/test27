


import { Component, OnInit } from '@angular/core';
import { UIRouter } from '@uirouter/angular';

@Component({
    templateUrl: './login.component.html',
    styleUrls: ['login.component.css']
})
export class loginComponent implements OnInit {
    constructor(private _uiRouter: UIRouter) { }

    credentials = { username: '', password: '' }
    errorMsg: string = '';
    ngOnInit() { }

    register() {
        this._uiRouter.stateService.go('register')
    }

    login() {

        let username = localStorage.getItem('login');
        let password = localStorage.getItem('password');
        if (this.credentials.username === username && this.credentials.password === password) {
            this._uiRouter.stateService.go('home')
        }
        else {
            this.errorMsg = " veuillez verifiez vos donées saisies "
        }
    }


}
