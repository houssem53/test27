import { Component, OnInit } from '@angular/core';
import { MdMenuModule } from '@angular/material';
import { MdSnackBar } from '@angular/material';
import { UIRouter } from '@uirouter/angular';

@Component({
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    ngOnInit() {
        let username = localStorage.getItem('login');
        this.openSnackBar('bienvenue', username);
    }
    constructor(public snackBar: MdSnackBar, private _uiRouter: UIRouter) { }

    openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action, {
            duration: 2000,
        });
    }
    logout() {
        localStorage.removeItem('login');
        localStorage.removeItem('password');
        this._uiRouter.stateService.go('login')
    }
}

