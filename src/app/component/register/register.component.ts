

import { Component, OnInit } from '@angular/core';
import { User } from '../../shared/model/user';
import { Router } from '@angular/router';
import { UIRouter } from '@uirouter/angular';

@Component({
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class registerComponent implements OnInit {


    constructor(private _uiRouter: UIRouter) { }
    user: User = { firstName: '', lastName: '', username: '', password: '' };

    ngOnInit() { }

    createUser() {
        localStorage.setItem('login', this.user.username);
        localStorage.setItem('password', this.user.password);
        this._uiRouter.stateService.go('login')
    }
}
