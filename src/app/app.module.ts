import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {NgModule, Component} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import { UIRouterModule } from '@uirouter/angular';
import { HomeComponent } from './component/home/home.component';
import { loginComponent } from './component/login/login.component';
import { registerComponent } from './component/register/register.component';
import { uiRouterConfigFn } from './app.routing';
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '@angular/material';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch'
import { MdButtonModule, MdCardModule, MdMenuModule, MdToolbarModule, MdIconModule } from '@angular/material';
import 'hammerjs';


let loginState = { name: 'login', url: '/login',  component: loginComponent };
let registerState = { name: 'register', url: '/register',  component: registerComponent };
let homeState = { name: 'home', url: '/home',  component: HomeComponent };


@NgModule({

  
  declarations: [
    AppComponent,HomeComponent,loginComponent,registerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterialModule,
    BrowserAnimationsModule,
    MdButtonModule,
    MdMenuModule,
    MdCardModule,
    MdToolbarModule,
    MdIconModule,
    UIRouterModule.forRoot({ states: [ loginState,registerState,homeState ], useHash: true ,  config: uiRouterConfigFn
})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
